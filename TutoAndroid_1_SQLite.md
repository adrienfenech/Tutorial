<p>Adrien Fenech</p>

<hr>



<h1 id="android-tuto-1-intégrer-sqlite-avec-ormlite">Android Tuto 1 : Intégrer SQLite avec ORMLite</h1>



<h2 id="introduction">Introduction</h2>

<p>Ce tutoriel nécessite les connaissances de bases en programmation avec le SDK Android, notamment l’ajout de librairies (Anglicisme de <em>Bibliothèques</em>), dans le projet. Ce tutoriel n’est pas l’unique possibilité, mais est une bonne base pour aborder le sujet des Bases de données avec Android.</p>

<blockquote>
  <p><i class="icon-checked"></i> Vous pouvez également consulter ce tutoriel via le <a href="https://stackedit.io/viewer#!provider=gist&gistId=4d8bbcb441b24f994f51&filename=Tuto_1.md">viewer de StackEdit</a><./p>
</blockquote>

<blockquote>
  <p><i class="icon-attention"></i> Ce tutoriel nécessite également des libs externes : <a href="http://ormlite.com/">ORMLite</a></p>
</blockquote>

<p><div class="toc"><div class="toc">
<ul>
<li><a href="#android-tuto-1-intégrer-sqlite-avec-ormlite">Android Tuto 1 : Intégrer SQLite avec ORMLite</a><ul>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#prérequis">Prérequis</a></li>
<li><a href="#partie-1-création-dune-table">Partie 1 : Création d’une table</a></li>
<li><a href="#partie-2-création-du-databasehelper">Partie 2 : Création du DatabaseHelper</a></li>
<li><a href="#partie-3-création-du-dbmanager">Partie 3 : Création du DBManager</a></li>
<li><a href="#partie-4-linkage-avec-un-autre-objet">Partie 4 : Linkage avec un autre objet</a></li>
<li><a href="#partie-5-exemple-dutilisation">Partie 5 : Exemple d’utilisation</a></li>
</ul>
</li>
</ul>
</div>
</div>
</p>



<h2 id="prérequis">Prérequis</h2>

<p>Après avoir créer votre projet, il est nécessaire d’ajouter 2 bibliothèques que vous pouvez récupérer via ce <a href="http://ormlite.com/releases/">lien</a>. <br>
Il suffit de télécharger les dernières versions de <strong>core</strong> et <strong>android</strong> au format <strong>jar</strong>. Ensuite, vous pouvez les inclure directement dans le projet.</p>

<blockquote>
  <p>Pour ceux qui ont oublié comment ajouter une librairie à un projet Android sous <strong>Android Studio</strong>, voici un petit rappel <i class="icon-smile"></i> :</p>
  
  <ul>
  <li>Vérifiez que le dossier <strong>libs</strong> situé dans le dossier <strong>app</strong> existe. Si ce n’est pas le cas, créez le.</li>
  <li>Ajoutez la(vos) bibliothèque(s) dans le dossier <strong>libs</strong>.</li>
  <li>Click droit sur la bibliothèque que vous désirez ajouter puis clickez sur <strong>Add as library…</strong> puis sur <strong>ok</strong> <br>
  Ca y est, votre bibliothèque est ajoutée, il ne vous reste plus qu’à <em>Rebuild</em> votre projet si ce n’est pas fait automatiquement.</li>
  </ul>
</blockquote>

<p>Maintenant que l’ajout des 2 bibliothèques est réalisé, plongeons nous dans l’implémentation de notre base de données.</p>



<h2 id="partie-1-création-dune-table">Partie 1 : Création d’une table</h2>

<p>Commençons par créer une table, qui représentera les informations d’une <em>personne</em>.</p>

<p>Créez tout d’abord un package <strong>model</strong> afin d’organiser son code. <br>
Créez-y la class <strong>Person</strong>. Voici la tête qu’aura notre class :</p>



<pre class="prettyprint"><code class=" hljs java"><span class="hljs-annotation">@DatabaseTable</span>(tableName = <span class="hljs-string">"person"</span>)
<span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">Person</span> {</span>
    <span class="hljs-keyword">public</span> <span class="hljs-title">Person</span>() {}
    <span class="hljs-keyword">public</span> <span class="hljs-title">Person</span>(String name, String sexe, <span class="hljs-keyword">int</span> age, <span class="hljs-keyword">int</span> pictureId) {
        <span class="hljs-keyword">this</span>.name = name;
        <span class="hljs-keyword">this</span>.sexe = sexe;
        <span class="hljs-keyword">this</span>.age = age;
        <span class="hljs-keyword">this</span>.pictureId = pictureId;
    }

    <span class="hljs-javadoc">/** Getter/Setter
    *
    */</span>

    <span class="hljs-keyword">public</span> <span class="hljs-keyword">long</span> <span class="hljs-title">getId</span>() {
        <span class="hljs-keyword">return</span> id;
    }

    <span class="hljs-keyword">public</span> <span class="hljs-keyword">int</span> <span class="hljs-title">getAge</span>() {
        <span class="hljs-keyword">return</span> age;
    }

    <span class="hljs-keyword">public</span> String <span class="hljs-title">getName</span>() {
        <span class="hljs-keyword">return</span> name;
    }

    <span class="hljs-keyword">public</span> String <span class="hljs-title">getSexe</span>() {
        <span class="hljs-keyword">return</span> sexe;
    }

    <span class="hljs-keyword">public</span> <span class="hljs-keyword">int</span> <span class="hljs-title">getPictureId</span>() {
        <span class="hljs-keyword">return</span> pictureId;
    }

    <span class="hljs-javadoc">/** A/D
     *
     */</span>

    <span class="hljs-annotation">@DatabaseField</span>(generatedId = <span class="hljs-keyword">true</span>)
    <span class="hljs-keyword">private</span> <span class="hljs-keyword">long</span> id;
    <span class="hljs-annotation">@DatabaseField</span>(columnName = <span class="hljs-string">"name"</span>, canBeNull = <span class="hljs-keyword">false</span>)
    <span class="hljs-keyword">private</span> String name;
    <span class="hljs-annotation">@DatabaseField</span>(columnName = <span class="hljs-string">"sexe"</span>, canBeNull = <span class="hljs-keyword">false</span>)
    <span class="hljs-keyword">private</span> String sexe;
    <span class="hljs-annotation">@DatabaseField</span>(columnName = <span class="hljs-string">"age"</span>, canBeNull = <span class="hljs-keyword">false</span>)
    <span class="hljs-keyword">private</span> <span class="hljs-keyword">int</span> age;
    <span class="hljs-annotation">@DatabaseField</span>(columnName = <span class="hljs-string">"pictureid"</span>, canBeNull = <span class="hljs-keyword">false</span>)
    <span class="hljs-keyword">private</span> <span class="hljs-keyword">int</span> pictureId;
}</code></pre>

<p>Vous l’avez deviné : la class vous allez utiliser dans votre projet et également celle qui servira de référence à votre table dans votre base de données. <br>
les annotations (<em>@something</em>) sont assez claires, donc je passerai rapidement dessus :</p>



<pre class="prettyprint"><code class=" hljs r">@DatabaseTable(tableName = <span class="hljs-string">"person"</span>)
public class Person {<span class="hljs-keyword">...</span>}</code></pre>

<p>Spécifie le nom de la table dans la base de donnée (ici <strong>person</strong>).</p>



<pre class="prettyprint"><code class=" hljs java"><span class="hljs-annotation">@DatabaseField</span>(generatedId = <span class="hljs-keyword">true</span>)
<span class="hljs-keyword">private</span> <span class="hljs-keyword">long</span> id;</code></pre>

<p><strong>id</strong> est l’identifiant qu’aura l’objet une fois dans la base de donnée. Ici, <strong>generatedId = true</strong> permettra d’attribuer automatiquement un identifiant à l’objet.</p>



<pre class="prettyprint"><code class=" hljs java"><span class="hljs-annotation">@DatabaseField</span>(columnName = <span class="hljs-string">"name"</span>, canBeNull = <span class="hljs-keyword">false</span>)
<span class="hljs-keyword">private</span> String name;</code></pre>

<p>Vous définissez ici le nom d’un des éléments de la table, ici <strong>name</strong>. Vous spécifiez également que cet élément ne peut pas être <em>null</em>. <br>
Il est important d’identifier grâce aux annotations chaque attribut de votre class.</p>

<blockquote>
  <p><i class="icon-attention"></i> Il est très important de créer explicitement un constructeur vide à votre class, afin que la base de données puissent s’initialiser.</p>
</blockquote>



<h2 id="partie-2-création-du-databasehelper">Partie 2 : Création du <em>DatabaseHelper</em></h2>

<p>On augmente un peu le niveau avec cette partie. Nous allons créer la class <strong>DatabaseHelper</strong> qui permettra de faire le lien avec la base de données.</p>

<p>Afin de mieux organiser son code, créez le <em>package</em> <strong>database</strong> et créez-y la class <strong>DatabaseHelper</strong>. Voici ce à quoi ressemblera notre class :</p>



<pre class="prettyprint"><code class=" hljs java"><span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">DatabaseHelper</span> <span class="hljs-keyword">extends</span> <span class="hljs-title">OrmLiteSqliteOpenHelper</span> {</span>
    <span class="hljs-keyword">public</span> <span class="hljs-title">DatabaseHelper</span>(Context context) {
        <span class="hljs-keyword">super</span>(context, DB_NAME, <span class="hljs-keyword">null</span>, DB_VERSION);
    }

    <span class="hljs-annotation">@Override</span>
    <span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">onCreate</span>(SQLiteDatabase sqLiteDatabase, com.j256.ormlite.support.ConnectionSource connectionSource) {
        <span class="hljs-keyword">try</span> {
            TableUtils.createTable(connectionSource, Person.class);
        } <span class="hljs-keyword">catch</span> (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    <span class="hljs-annotation">@Override</span>
    <span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">onUpgrade</span>(SQLiteDatabase sqLiteDatabase, com.j256.ormlite.support.ConnectionSource connectionSource, <span class="hljs-keyword">int</span> i, <span class="hljs-keyword">int</span> i2) {
        <span class="hljs-keyword">try</span> {
            TableUtils.dropTable(connectionSource, Person.class, <span class="hljs-keyword">true</span>);
        } <span class="hljs-keyword">catch</span> (SQLException e) {
            e.printStackTrace();
        }
    }

    <span class="hljs-javadoc">/** Getter/Setter
     *
     */</span>
    <span class="hljs-keyword">public</span> Dao&lt;Person, Integer&gt; <span class="hljs-title">getPersonDao</span>() {
        <span class="hljs-keyword">if</span> (personDAO == <span class="hljs-keyword">null</span>) {
            <span class="hljs-keyword">try</span> {
               personDAO = getDao(Person.class);
            }<span class="hljs-keyword">catch</span> (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        <span class="hljs-keyword">return</span> personDAO;
    }

    <span class="hljs-javadoc">/** A/D
     *
     */</span>
    <span class="hljs-keyword">private</span> Dao&lt;Person, Integer&gt;                    personDAO= <span class="hljs-keyword">null</span>;


    <span class="hljs-javadoc">/** CONSTANT
     *
     */</span>
    <span class="hljs-keyword">private</span> <span class="hljs-keyword">static</span> <span class="hljs-keyword">final</span> String     DB_NAME = <span class="hljs-string">"myDatabase.sqlite"</span>;
    <span class="hljs-keyword">private</span> <span class="hljs-keyword">static</span> <span class="hljs-keyword">final</span> <span class="hljs-keyword">int</span>        DB_VERSION = <span class="hljs-number">3</span>;
}</code></pre>

<p>Passons en revue les différentes parties de cette class: <br>
Tout d’abord les attributs et constantes qui sont utilisés dans le constructeur.</p>



<pre class="prettyprint"><code class=" hljs java"><span class="hljs-javadoc">/** A/D
*
*/</span>
<span class="hljs-keyword">private</span> Dao&lt;Person, Integer&gt; personDAO = <span class="hljs-keyword">null</span>;</code></pre>

<p>Ce sera un DAO qui s’occupera de faire la liaison entre la base de données et les différentes méthodes d’accès/modifications des données que vous voudrez implémenter par la suite.</p>



<pre class="prettyprint"><code class=" hljs java"><span class="hljs-javadoc">/** CONSTANT
*
*/</span>
<span class="hljs-keyword">private</span> <span class="hljs-keyword">static</span> <span class="hljs-keyword">final</span> String DB_NAME = <span class="hljs-string">"myDatabase.sqlite"</span>;
    <span class="hljs-keyword">private</span> <span class="hljs-keyword">static</span> <span class="hljs-keyword">final</span> <span class="hljs-keyword">int</span> DB_VERSION = <span class="hljs-number">3</span>;</code></pre>

<p>Ici, nous spécifions simplement le nom de la base de données et sa version.</p>

<p>Ensuite, voyons l’initialisation avec les  méthodes <strong>onCreate</strong> et <strong>onUpgrade</strong>.</p>



<pre class="prettyprint"><code class=" hljs r">@Override
public void onCreate(<span class="hljs-keyword">...</span>) {
    <span class="hljs-keyword">...</span>
    TableUtils.createTable(connectionSource, Person.class);
    <span class="hljs-keyword">...</span>
}</code></pre>

<p>Cette méthode va créer la table de la class <strong>Person</strong>, tout simplement. <br>
Il vous faudra suivre cette exemple si vous voulez implémentez une nouvelle table.</p>



<pre class="prettyprint"><code class=" hljs r">@Override
public void onUpgrade(<span class="hljs-keyword">...</span>) {
    <span class="hljs-keyword">...</span>
    TableUtils.dropTable(connectionSource, Person.class, true);
    <span class="hljs-keyword">...</span>
}</code></pre>

<p>Cette méthode va upgrader la table de la class <strong>Person</strong>. <br>
Il vous faudra également suivre cet exemple si vous voulez implémenter une nouvelle table.</p>

<p>La méthode d’accès au DAO de la table <strong>person</strong> est transparente quant à sa compréhension.</p>



<h2 id="partie-3-création-du-dbmanager">Partie 3 : Création du DBManager</h2>

<p>Nous allons maintenant créer (toujours dans le package <strong>database</strong>) le manager qui va nous permettre de créer toutes nos méthodes d’accès à notre table <strong>person</strong> via la class <strong>DatabaseHelper</strong> implémentée précédemment.</p>

<p>La class se nommera <strong>DBManager</strong> (pour <em>DatabaseManager</em>) et sera un <strong>singleton</strong> afin de pouvoir récupérer le manager n’importe où dans votre projet. En voici le code :</p>



<pre class="prettyprint"><code class=" hljs java"><span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">DBManager</span> {</span>
    <span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> <span class="hljs-keyword">void</span> <span class="hljs-title">Init</span>(Context context) {
        <span class="hljs-keyword">if</span> (ourInstance == <span class="hljs-keyword">null</span>)
            ourInstance = <span class="hljs-keyword">new</span> DBManager(context);
    }

    <span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> DBManager <span class="hljs-title">getInstance</span>() {
        <span class="hljs-keyword">return</span> ourInstance;
    }

    <span class="hljs-keyword">private</span> <span class="hljs-title">DBManager</span>(Context context) {
        helper = <span class="hljs-keyword">new</span> DatabaseHelper(context);
    }

    <span class="hljs-keyword">private</span> DatabaseHelper <span class="hljs-title">getHelper</span>() {
        <span class="hljs-keyword">return</span> helper;
    }

    <span class="hljs-javadoc">/** Methods [Person] **/</span>

    <span class="hljs-keyword">public</span> List&lt;Person&gt; <span class="hljs-title">getAllPerson</span>() {
        <span class="hljs-keyword">try</span> {
            <span class="hljs-keyword">return</span> getHelper().getPersonDao().queryForAll();
        } <span class="hljs-keyword">catch</span> (SQLException e) {
            e.printStackTrace();
            <span class="hljs-keyword">return</span> <span class="hljs-keyword">new</span> ArrayList&lt;Person&gt;();
        }
    }

    <span class="hljs-keyword">public</span> <span class="hljs-keyword">long</span> <span class="hljs-title">createPerson</span>(Person person) {
        <span class="hljs-keyword">try</span> {
            getHelper().getPersonDao().create(person);
            <span class="hljs-keyword">return</span> person.getId();
        } <span class="hljs-keyword">catch</span> (SQLException e) {
            e.printStackTrace();
            <span class="hljs-keyword">return</span> -<span class="hljs-number">1</span>;
        }
    }

    <span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">removePersons</span>(Collection&lt;Person&gt; persons) {
        <span class="hljs-keyword">try</span> {
            getHelper().getPersonDao().delete(persons);
        } <span class="hljs-keyword">catch</span> (SQLException e) {
            e.printStackTrace();
        }
    }

    <span class="hljs-keyword">public</span> <span class="hljs-keyword">long</span> <span class="hljs-title">updatePerson</span>(Person person) {
        <span class="hljs-keyword">try</span> {
            getHelper().getPersonDao().update(person);
            <span class="hljs-keyword">return</span> person.getId();
        } <span class="hljs-keyword">catch</span> (SQLException e) {
            e.printStackTrace();
            <span class="hljs-keyword">return</span> -<span class="hljs-number">1</span>;
        }
    }

    <span class="hljs-keyword">public</span> List&lt;Person&gt; <span class="hljs-title">getPersonByName</span>(String name) {
        <span class="hljs-keyword">try</span> {
            <span class="hljs-keyword">return</span> getHelper().getPersonDao().queryForEq(<span class="hljs-string">"name"</span>, name);
        } <span class="hljs-keyword">catch</span> (SQLException e) {
            e.printStackTrace();
            <span class="hljs-keyword">return</span> <span class="hljs-keyword">new</span> List&lt;Person&gt;();
        }
    }

  <span class="hljs-javadoc">/** A/D
     *
     */</span>
    <span class="hljs-keyword">private</span> DatabaseHelper  helper;
    <span class="hljs-keyword">private</span> <span class="hljs-keyword">static</span> DBManager ourInstance;
}</code></pre>

<p>Comme vous pouvez le constater, notre DBManager est bel et bien un singleton, et il nous suffit simplement d’initialiser notre <strong>DatabaseHelper</strong> implémenté précédemment.</p>

<pre class="prettyprint"><code class=" hljs java"><span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> <span class="hljs-keyword">void</span> <span class="hljs-title">Init</span>(Context context) {
    <span class="hljs-keyword">if</span> (ourInstance == <span class="hljs-keyword">null</span>)
        ourInstance = <span class="hljs-keyword">new</span> DBManager(context);
}

<span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> DBManager <span class="hljs-title">getInstance</span>() {
    <span class="hljs-keyword">return</span> ourInstance;
}

<span class="hljs-keyword">private</span> <span class="hljs-title">DBManager</span>(Context context) {
    helper = <span class="hljs-keyword">new</span> DatabaseHelper(context);
}

<span class="hljs-keyword">private</span> DatabaseHelper <span class="hljs-title">getHelper</span>() {
    <span class="hljs-keyword">return</span> helper;
}

   <span class="hljs-javadoc">/** ... **/</span>

<span class="hljs-keyword">private</span> DatabaseHelper  helper;
<span class="hljs-keyword">private</span> <span class="hljs-keyword">static</span> DBManager ourInstance;</code></pre>

<p>Ici, on initialise notre DBManager qu’une seule et unique fois, et ce même si la méthode d’initialisation est appelée plusieurs fois. Nous avons aussi notre méthode d’accès <em>public</em> au <strong>DBManager</strong> ainsi que notre méthode <em>privée</em> d’accès au <strong>DatabaseHelper</strong> pour toute la partie interne à notre class.</p>

<p>Voyons maintenant les différentes méthodes d’accès à notre table:</p>



<pre class="prettyprint"><code class=" hljs cs"><span class="hljs-keyword">public</span> <span class="hljs-keyword">long</span> <span class="hljs-title">createPerson</span>(Person person) {
    <span class="hljs-keyword">try</span> {
        getHelper().getPersonDao().create(person);
        <span class="hljs-keyword">return</span> person.getId();
    } <span class="hljs-keyword">catch</span> (SQLException e) {
        e.printStackTrace();
        <span class="hljs-keyword">return</span> -<span class="hljs-number">1</span>;
    }
}</code></pre>

<p>Grâce à la méthode <em>createPerson</em>, nous ajoutons l’objet <em>person</em> à la base de données. On remarquera qu’il faut passer par le DAO créé précédemment et sa méthode <em>create</em>.</p>



<pre class="prettyprint"><code class=" hljs cs"><span class="hljs-keyword">public</span> List&lt;Person&gt; <span class="hljs-title">getAllPerson</span>() {
    <span class="hljs-keyword">try</span> {
        <span class="hljs-keyword">return</span> getHelper().getPersonDao().queryForAll();
    } <span class="hljs-keyword">catch</span> (SQLException e) {
        e.printStackTrace();
        <span class="hljs-keyword">return</span> <span class="hljs-keyword">new</span> ArrayList&lt;Person&gt;();
    }
}

<span class="hljs-keyword">public</span> List&lt;Person&gt; <span class="hljs-title">getPersonByName</span>(String name) {
    <span class="hljs-keyword">try</span> {
        <span class="hljs-keyword">return</span> getHelper().getPersonDao().queryForEq(<span class="hljs-string">"name"</span>, name);
    } <span class="hljs-keyword">catch</span> (SQLException e) {
        e.printStackTrace();
        <span class="hljs-keyword">return</span> <span class="hljs-keyword">new</span> List&lt;Person&gt;();
    }
}</code></pre>

<p>Voici 2 méthodes d’accès à notre table <em>person</em>.  <br>
La première, grâce la méthode du DAO <em>queryForAll</em> permet de récupérer toutes les personnes de la table en question. <br>
La seconde permet de récupérer les personnes avec une condition. Ici, suivant le <em>nom</em> de la personne (tout comme un <em>WHERE</em> dans les bases de données). La méthode <em>queryForEq</em> du DAO permet de lister uniquement les personnes ayant pour nom “<em>name</em>“;</p>

<p>Enfin, les deux dernières méthodes présentées :</p>



<pre class="prettyprint"><code class=" hljs cs"><span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">removePersons</span>(Collection&lt;Person&gt; persons) {
    <span class="hljs-keyword">try</span> {
        getHelper().getPersonDao().delete(persons);
    } <span class="hljs-keyword">catch</span> (SQLException e) {
        e.printStackTrace();
    }
}

<span class="hljs-keyword">public</span> <span class="hljs-keyword">long</span> <span class="hljs-title">updatePerson</span>(Person person) {
    <span class="hljs-keyword">try</span> {
        getHelper().getPersonDao().update(person);
        <span class="hljs-keyword">return</span> person.getId();
    } <span class="hljs-keyword">catch</span> (SQLException e) {
        e.printStackTrace();
        <span class="hljs-keyword">return</span> -<span class="hljs-number">1</span>;
    }
}</code></pre>

<p>Pas besoin de rester sur ces méthodes qui parlent d’elles mêmes :  <br>
La première pour supprimer des personnes de la base de données, la deuxième pour actualiser une personne déjà présente dans la base de données.</p>

<blockquote>
  <p><i class="icon-attention"></i> Comme vous l’avez certainement remarqué, les méthodes <strong>createPerson</strong> et <strong>updatePerson</strong> retournent un <strong>long</strong>. Ce nombre correspond à l’identifiant unique attribué à l’objet entré dans la base de données. Il vous permettra de linker avec un éventuel autre objet.</p>
</blockquote>



<h2 id="partie-4-linkage-avec-un-autre-objet">Partie 4 : Linkage avec un autre objet</h2>

<p>Nous allons voir maintenant comment linker 2 tables entre elles. Dans notre cas, c’est très simple : Il suffit de créer un objet qui contiendra les identifiants des 2 objets que nous allons lier.</p>

<p>Tout d’abord, créez dans le <em>package</em> <strong>model</strong> une class <strong>MyEvent</strong> qui correspondra à l’objet événement auquel nos personnes participeront.</p>

<blockquote>
  <p><i class="icon-attention"></i> Veillez à bien respecter le modèle que nous avons vu pour la class <strong>Person</strong>, avec bien entendu différents attributs. Seul l’attribut <strong>id</strong> doit être présent afin de garder l’identifiant de notre événement.</p>
</blockquote>

<p>Ensuite, créez la class <strong>MyEventPerson</strong> qui correspondra au lien entre notre événement et nos personnes. En voici les attributs :</p>



<pre class="prettyprint"><code class=" hljs java"><span class="hljs-javadoc">/** A/D
 *
 */</span>
<span class="hljs-annotation">@DatabaseField</span>(generatedId = <span class="hljs-keyword">true</span>)
<span class="hljs-keyword">private</span> <span class="hljs-keyword">long</span> id;
<span class="hljs-annotation">@DatabaseField</span>(columnName = <span class="hljs-string">"myeventid"</span>, canBeNull = <span class="hljs-keyword">false</span>)
<span class="hljs-keyword">private</span> <span class="hljs-keyword">long</span> myEvent_ID;
<span class="hljs-annotation">@DatabaseField</span>(columnName = <span class="hljs-string">"personid"</span>, canBeNull = <span class="hljs-keyword">false</span>)
<span class="hljs-keyword">private</span> <span class="hljs-keyword">long</span> person_ID;</code></pre>

<p>Notre attribut <strong>id</strong> est toujours présent afin de garder l’identifiant de notre lien. <br>
Deux attributs sont également présents : <strong>myEvent_ID</strong> et <strong>person_ID</strong> (correspondant respectivement à l’identifiant de note événement et celui de notre personne).</p>

<p>Retour maintenant dans notre class <strong>DBManager</strong> où nous allons ajouter ces 3 méthodes :</p>



<pre class="prettyprint"><code class=" hljs java">    <span class="hljs-javadoc">/** Methods [MyEventPerson] **/</span>

<span class="hljs-keyword">public</span> List&lt;MyEventPerson&gt; <span class="hljs-title">getAllMyEventPerson</span>() {
    <span class="hljs-keyword">try</span> {
        <span class="hljs-keyword">return</span> getHelper().getMyEventPersonDao().queryForAll();
    } <span class="hljs-keyword">catch</span> (SQLException e) {
        e.printStackTrace();
        <span class="hljs-keyword">return</span> <span class="hljs-keyword">new</span> ArrayList&lt;MyEventPerson&gt;();
    }
}

<span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">createMyEventPerson</span>(MyEventPerson myEventPerson) {
    <span class="hljs-keyword">try</span> {
        getHelper().getMyEventPersonDao().create(myEventPerson);
    } <span class="hljs-keyword">catch</span> (SQLException e) {
        e.printStackTrace();
    }
}

<span class="hljs-keyword">public</span> List&lt;Person&gt; <span class="hljs-title">getPeopleFromMyEvent</span>(<span class="hljs-keyword">long</span> myEvent_ID) {
    <span class="hljs-keyword">try</span> {
        List&lt;MyEventPerson&gt; temp = getHelper().getMyEventPersonDao().queryForEq(<span class="hljs-string">"myeventid"</span>, myEvent_ID);
        ArrayList&lt;Person&gt; res = <span class="hljs-keyword">new</span> ArrayList&lt;Person&gt;();
        <span class="hljs-keyword">for</span> (MyEventPerson myEventPerson : temp)
            res.add(getHelper().getPersonDao().queryForId((<span class="hljs-keyword">int</span>)myEventPerson.getPerson_ID()));
        <span class="hljs-keyword">return</span> res;
    } <span class="hljs-keyword">catch</span> (SQLException e) {
        e.printStackTrace();
        <span class="hljs-keyword">return</span> <span class="hljs-keyword">new</span> ArrayList&lt;Person&gt;();
    }
}</code></pre>

<p>Les méthodes <strong>getAllMyEventPerson</strong> et <strong>createMyEventPerson</strong> parlent d’elles mêmes. La méthode intéressante ici est <strong>getPeopleFromMyEvent</strong>. <br>
Nous récupérons d’abord chaque objet <strong>MyEventPerson</strong> ayant pour <strong>myeventid</strong> l’identifiant de l’événement en question. <br>
Ensuite, pour chaque objet de la liste récupérée, nous obtenons les identifiants des personnes associées. Il suffit donc simplement de récupérer ces personnes grâce à la méthode <em>queryForId</em> du <strong>personDAO</strong>.</p>

<blockquote>
  <p><i class="icon-attention"></i> Il vous faut également ajouter les méthodes en rapport avec la class <strong>MyEvent</strong> tel que <strong>createMyEvent</strong> etc… et le DAO associé dans le <strong>DatabaseHelper</strong>.</p>
</blockquote>



<h2 id="partie-5-exemple-dutilisation">Partie 5 : Exemple d’utilisation</h2>

<p>Il est temps maintenant d’utiliser ces tables. Voici une méthode :</p>



<pre class="prettyprint"><code class=" hljs java"><span class="hljs-javadoc">/** ... **/</span>

DBManager manager = DBManager.getInstance();

<span class="hljs-keyword">long</span> id1 = manager.createPerson(<span class="hljs-keyword">new</span> Person(<span class="hljs-string">"Toto"</span>, <span class="hljs-string">"M"</span>, <span class="hljs-number">14</span>, <span class="hljs-number">0</span>));
<span class="hljs-keyword">long</span> id2 = manager.createPerson(<span class="hljs-keyword">new</span> Person(<span class="hljs-string">"Jean"</span>, <span class="hljs-string">"M"</span>, <span class="hljs-number">18</span>, <span class="hljs-number">0</span>));
<span class="hljs-keyword">long</span> id3 = manager.createPerson(<span class="hljs-keyword">new</span> Person(<span class="hljs-string">"Mathilde"</span>, <span class="hljs-string">"F"</span>, <span class="hljs-number">17</span>, <span class="hljs-number">0</span>));
<span class="hljs-keyword">long</span> id4 = manager.createPerson(<span class="hljs-keyword">new</span> Person(<span class="hljs-string">"Laura"</span>, <span class="hljs-string">"F"</span>, <span class="hljs-number">22</span>, <span class="hljs-number">0</span>));
<span class="hljs-keyword">long</span> id5 = manager.createPerson(<span class="hljs-keyword">new</span> Person(<span class="hljs-string">"Fred"</span>, <span class="hljs-string">"M"</span>, <span class="hljs-number">18</span>, <span class="hljs-number">0</span>));

<span class="hljs-keyword">long</span> lastMyEventId = -<span class="hljs-number">1</span>;
MyEvent myEvent = <span class="hljs-keyword">new</span> MyEvent(<span class="hljs-string">"Birthday"</span>);
lastMyEventId = manager.createMyEvent(myEvent);

<span class="hljs-keyword">if</span> (lastMyEventId != -<span class="hljs-number">1</span>) {
    manager.createMyEventPerson(<span class="hljs-keyword">new</span> MyEventPerson(lastMyEventId, id2));
    manager.createMyEventPerson(<span class="hljs-keyword">new</span> MyEventPerson(lastMyEventId, id3));
    manager.createMyEventPerson(<span class="hljs-keyword">new</span> MyEventPerson(lastMyEventId, id4));
}
<span class="hljs-keyword">else</span>
    System.out.println(<span class="hljs-string">"Wrong MyEvent_ID"</span>);

<span class="hljs-javadoc">/** ... **/</span></code></pre>

<p>Et voilà, c’est aussi simple que ça pour l’utilisation. Bien qu’il y ait beaucoup de codes en amont, il est facilement réutilisable avec seulement quelques modifications.</p>

<p>N’hésitez pas à faire le tour de ORMLite afin de voir toutes les possibilités. Il ne s’agit ici que d’une brève introduction et une approche assez grossière afin de comprendre le fonctionnement.</p>

<hr>

<p>Adrien Fenech</p>
