<p>Adrien Fenech</p>

<hr>



<h1 id="android-tuto-2-integrer-des-achats-inapp">Android Tuto 2 : Intégrer des achats InApp</h1>



<h2 id="introduction">Introduction</h2>

<p>Ce tutoriel nécessite les connaissances de base en programmation avec le SDK Android, notamment l’ajout de librairies (Anglicisme de <em>Bibliothèques</em>), dans le projet. Ce tutoriel est une version simplifiée de celui proposé en référence sur <a href="http://developer.android.com/google/play/billing/billing_integrate.html">developer android</a>.</p>

<blockquote>
  <p><i class="icon-checked"></i> Vous pouvez également consulter ce tutoriel via le <a href="https://stackedit.io/viewer#!provider=gist&gistId=a477de4d16b118402faf&filename=TutoAndroid_2_InAppBilling.md">viewer de StackEdit</a><./p>
</blockquote>

<blockquote>
  <p><i class="icon-attention"></i> Ce tutoriel nécessite la mise en place du fichier <strong>AIDL</strong> que j’expliquerai pour <strong>Android Studio</strong>.</p>
</blockquote>

<p><div class="toc"><div class="toc">
<ul>
<li><a href="#android-tuto-2-integrer-des-achats-inapp">Android Tuto 2 : Intégrer des achats InApp</a><ul>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#partie-1-ajout-du-fichier-aidl">Partie 1 : Ajout du fichier AIDL</a></li>
<li><a href="#partie-2-creation-dun-item-sur-le-google-play-store">Partie 2 : Création d’un item sur le Google Play Store</a></li>
<li><a href="#partie-3-creation-du-billingmanager">Partie 3 : Création du BillingManager</a><ul>
<li><a href="#etape-1-initialisation">Etape 1 : Initialisation</a></li>
<li><a href="#etape-2-envoie-dune-requete-dachat">Etape 2 : Envoie d’une requête d’achat</a></li>
<li><a href="#etape-3-reception-de-la-réponse-serveur">Etape 3 : Réception de la réponse serveur</a></li>
<li><a href="#etape-bonus-methode-utile">Etape bonus : Méthode utile</a></li>
</ul>
</li>
<li><a href="#partie-4-exemple-dutilisation">Partie 4 : Exemple d’utilisation</a></li>
</ul>
</li>
</ul>
</div>
</div>
</p>

<h2 id="partie-1-ajout-du-fichier-aidl">Partie 1 : Ajout du fichier <strong>AIDL</strong></h2>

<p>Cette partie explique comment inclure le fichier <strong>AIDL</strong> à notre projet sur <strong>Android Studio</strong>.</p>

<blockquote>
  <p><strong>AIDL</strong> est un acronyme pour <em>Android Interface Definition Language</em> qui n’est autre que l’interface avec laquelle nous allons effectuer nos méthodes d’achats.</p>
</blockquote>

<ul>
<li>Ouvrez tout d’abord le <strong>Android SDK Manager</strong> (<em>Tools</em> -&gt; <em>Android</em> -&gt; <em>SDK Manager</em>)</li>
<li>Sélectionnez <strong>Google Play Billing Library</strong> et cliquez sur <strong>install packages</strong>.</li>
</ul>

<p>Vous avez normalement un fichier nommé <strong>IInAppBillingService.aidl</strong> qui s’est installé. Il se situe dans votre dossier du SDK Android (que vous avez installé avec Android Studio par exemple). Il se situe sous <em>extras/google/play_billing</em>.</p>

<p>Il faut maintenant l’ajouter au projet. Pour cela : <br>
* Créez tout d’abord un dossier <strong>aidl</strong> sous <em>app/src/main/</em>. <br>
* Dans ce dossier, créez le package <em>com.android.vending.billing</em>. <br>
* Importez le fichier <strong>IInAppBillingService.aidl</strong> dans ce dernier. <br>
* <em>Rebuildez</em> votre projet. Si tout s’est bien déroulé, une class <strong>IInAppBillingService.java</strong> est apparue dans le dossier <em>gen</em> ou <em>generated</em> de votre projet. Si c’est le cas, alors vous avez bien intégré <strong>IInAppBillingService.aidl</strong> à votre projet.</p>

<blockquote>
  <p>Avant de continuer, pensez dès à présent à modifier votre <strong>AndroidManifest.xml</strong> en y ajoutant l’autorisation d’achat :</p>
  
  <pre class="prettyprint"><code class=" hljs xml"><span class="hljs-tag">&lt;<span class="hljs-title">uses-permission</span> <span class="hljs-attribute">android:name</span>=<span class="hljs-value">"com.android.vending.BILLING"</span> /&gt;</span></code></pre>
</blockquote>

<h2 id="partie-2-creation-dun-item-sur-le-google-play-store">Partie 2 : Création d’un item sur le <em>Google Play Store</em></h2>

<p>Nous allons voir ensemble comment créer un item sur le <em>Google Play Store</em>. </p>

<blockquote>
  <p>Pour accéder à cette partie, vous devez avoir créé au préalable la fiche de votre projet et rempli les différentes conditions qui permettent de toucher à l’argent sur votre compte (configuration du compte <em>Google Marchand</em> par exemple).</p>
</blockquote>

<ul>
<li>Une fois votre projet sélectionné, cliquez sur <em>Produits intégrés à l’application</em>.</li>
<li>Cliquez sur <em>Ajouter un nouveau produit</em>. </li>
<li>Sélectionnez <em>Produit géré</em> (Ou <em>Produit non géré</em>, cela n’a plus d’importance)</li>
<li>Renseignez un identifiant puis cliquez sur <em>Continuer</em></li>
<li>Renseignez un <em>Titre</em>, une <em>Description</em> ainsi qu’un <em>prix par défaut</em>.</li>
<li>Cliquez sur <em>Convertir les prix automatiquement</em> afin de ne pas avoir à entrer manuellement le prix dans toutes les devises.</li>
<li>Enfin, cliquez sur le bouton <em>Inactif</em> puis sur <em>Activer</em>.</li>
</ul>

<p>Votre item est maintenant créé !</p>

<blockquote>
  <p><i class="icon-attention"></i> Plusieurs heures peuvent s’écouler avant que le produit soit actif.</p>
</blockquote>

<h2 id="partie-3-creation-du-billingmanager">Partie 3 : Création du <strong>BillingManager</strong></h2>

<p>Nous allons créer un <em>manager</em> qui sera en charge de réaliser toutes nos actions en rapport avec l’achat <em>InApp</em>.</p>



<h3 id="etape-1-initialisation">Etape 1 : Initialisation</h3>

<p>Pour cela, créez un <em>package</em> <strong>inappbilling</strong>. Et créez-y la class <strong>BillingManager</strong> qui aura la forme d’un singleton. En voici la forme :</p>

<pre class="prettyprint"><code class=" hljs cs"><span class="hljs-keyword">public</span> <span class="hljs-keyword">class</span> BillingManager {
    <span class="hljs-keyword">private</span> <span class="hljs-keyword">static</span> BillingManager ourInstance = <span class="hljs-keyword">new</span> BillingManager();
    <span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> BillingManager <span class="hljs-title">getInstance</span>() {
        <span class="hljs-keyword">return</span> ourInstance;
    }

    <span class="hljs-keyword">private</span> <span class="hljs-title">BillingManager</span>() {
    }
}</code></pre>

<p>Pas besoin de passer en revue ce bout de code, il s’agit de l’architecture type d’un <em>singleton</em>.</p>

<p>Nous allons nous occuper en premier lieu de l’initialisation de l’<em>InApp</em> et la connexion du service. Voici la méthode à ajouter dans votre class :</p>

<pre class="prettyprint"><code class=" hljs java"><span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">Initialize</span>() {
    <span class="hljs-comment">// Payment etc</span>
    ServiceConnection mServiceConn = <span class="hljs-keyword">new</span> ServiceConnection() {
        <span class="hljs-annotation">@Override</span>
        <span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">onServiceDisconnected</span>(ComponentName name) {
            mService = <span class="hljs-keyword">null</span>;
        }
        <span class="hljs-annotation">@Override</span>
        <span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">onServiceConnected</span>(ComponentName name,
                                           IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
            <span class="hljs-comment">// Do something here</span>
        }
    };

    Intent serviceIntent = <span class="hljs-keyword">new</span> Intent(<span class="hljs-string">"com.android.vending.billing.InAppBillingService.BIND"</span>);
    serviceIntent.setPackage(<span class="hljs-string">"com.android.vending"</span>);
    activity.bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
}

<span class="hljs-javadoc">/** A/D
*
*/</span>
IInAppBillingService mService;</code></pre>

<p>Nous créons ici la connexion avec le <em>Google Play Store</em>. Vous pouvez également ajouter dans la méthode <strong>onServiceConnected</strong> des tests avec vos achats réalisés etc.. Je donnerai un exemple peu après.</p>



<h3 id="etape-2-envoie-dune-requete-dachat">Etape 2 : Envoie d’une requête d’achat</h3>

<p>Il est temps de créer une méthode pour acheter un service dans votre application. Pour cela, nous allons créer une méthode <strong>tryToPurchase</strong>. Cette dernière permettra de réaliser un achat grâce à l’identifiant donné en argument.</p>

<pre class="prettyprint"><code class=" hljs java"><span class="hljs-keyword">protected</span> <span class="hljs-keyword">void</span> <span class="hljs-title">tryToPurchase</span>(String idProduct) {
    Bundle buyIntentBundle = <span class="hljs-keyword">null</span>;
    <span class="hljs-keyword">try</span> {
        buyIntentBundle = mService.getBuyIntent(<span class="hljs-number">3</span>, activity.getPackageName(), idProduct, <span class="hljs-string">"inapp"</span>, <span class="hljs-string">"bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ"</span>);
        PendingIntent pendingIntent = buyIntentBundle.getParcelable(<span class="hljs-string">"BUY_INTENT"</span>);
        <span class="hljs-keyword">if</span> (pendingIntent == <span class="hljs-keyword">null</span>) {
            <span class="hljs-keyword">return</span>;
        }
            activity.startIntentSenderForResult(pendingIntent.getIntentSender(), <span class="hljs-number">1001</span>, <span class="hljs-keyword">new</span> Intent(), Integer.valueOf(<span class="hljs-number">0</span>), Integer.valueOf(<span class="hljs-number">0</span>), Integer.valueOf(<span class="hljs-number">0</span>));
    } <span class="hljs-keyword">catch</span> (RemoteException e) {
            e.printStackTrace();
    } <span class="hljs-keyword">catch</span> (IntentSender.SendIntentException e) {
            e.printStackTrace();
    }
}

<span class="hljs-javadoc">/** Getter/Setter
*
*/</span>
Public <span class="hljs-keyword">void</span> setActivity(Activity activity) {
    <span class="hljs-keyword">this</span>.activity = activity;
}

<span class="hljs-javadoc">/** A/D
*
*/</span>
<span class="hljs-keyword">private</span> Activity activity;</code></pre>

<p>Ici, nous avons simplement créé un <em>Bundle</em> qui s’occupera de faire le lien entre le <em>Google Play Store</em> et nous, afin d’envoyer la requête d’achat. Il faut bien veiller à ajouter l’identifiant de l’item à acheter (ici <strong>idProduct</strong>). Nous précisons ensuite qu’il s’agit bien d’un achat grâce à <strong>BUY_INTENT</strong>. Puis nous envoyons notre requête ensuite avec la méthode <strong>startIntentSenderForResult</strong>.</p>

<h3 id="etape-3-reception-de-la-réponse-serveur">Etape 3 : Réception de la réponse serveur</h3>

<p>Il ne nous reste plus qu’à connaître la réponse du serveur. Pour cela, c’est la méthode <strong>onActivityResult</strong> de l’activité <strong>activity</strong> qui a servi à envoyer la requête qui sera automatiquement appelée. Il suffit donc de l’<em>overrider</em>. </p>

<p>Dans la class de votre activité, implémentez la méthode <strong>onActivityResult</strong> :</p>

<pre class="prettyprint"><code class=" hljs java"><span class="hljs-annotation">@Override</span>
<span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">onActivityResult</span>(<span class="hljs-keyword">int</span> requestCode, <span class="hljs-keyword">int</span> resultCode, Intent data) {
    <span class="hljs-keyword">if</span> (requestCode == <span class="hljs-number">1001</span>) {
        <span class="hljs-keyword">int</span> responseCode = data.getIntExtra(<span class="hljs-string">"RESPONSE_CODE"</span>, <span class="hljs-number">0</span>);
        String purchaseData = data.getStringExtra(<span class="hljs-string">"INAPP_PURCHASE_DATA"</span>);
        String dataSignature = data.getStringExtra(<span class="hljs-string">"INAPP_DATA_SIGNATURE"</span>);

        <span class="hljs-keyword">if</span> (resultCode == Activity.RESULT_OK) {
            <span class="hljs-keyword">try</span> {
                JSONObject jo = <span class="hljs-keyword">new</span> JSONObject(purchaseData);
                String sku = jo.optString(<span class="hljs-string">"productId"</span>);
                String packageName = jo.optString(<span class="hljs-string">"packageName"</span>);
                String purchaseToken = jo.optString(<span class="hljs-string">"purchaseToken"</span>);

                <span class="hljs-keyword">if</span> (sku.equalsIgnoreCase(<span class="hljs-string">"myItem1"</span>)) {
                    <span class="hljs-comment">// Do something here...</span>
                    <span class="hljs-comment">//int response = BillingManager.getInstance().getmService().consumePurchase(3, packageName, purchaseToken);</span>
                }
                <span class="hljs-keyword">else</span> <span class="hljs-keyword">if</span> (sku.equalsIgnoreCase(<span class="hljs-string">"myItem2"</span>)) {
                    <span class="hljs-comment">// Do something here...</span>
                    <span class="hljs-comment">//int response = BillingManager.getInstance().getmService().consumePurchase(3, packageName, purchaseToken);</span>
                }
            } <span class="hljs-keyword">catch</span> (JSONException e) {
                e.printStackTrace();
            } <span class="hljs-keyword">catch</span> (Exception e) {
                e.printStackTrace();
            }
        }
    }
}</code></pre>

<p>Vous pouvez soit tout implémenter dans votre activité, soit tout mettre dans le <em>manager</em> que vous avez créé précédemment (afin d’obtenir un code un peu plus propre) et faire une simple redirection de l’activité vers le <em>manager</em>, de la forme : </p>



<pre class="prettyprint"><code class=" hljs java"><span class="hljs-annotation">@Override</span>
<span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">onActivityResult</span>(<span class="hljs-keyword">int</span> requestCode, <span class="hljs-keyword">int</span> resultCode, Intent data) {
    BillingManager.getInstance().onActivityResult(requestCode, resultCode, data);
}</code></pre>

<p>Votre code sera alors mieux organisé (chaque chose à sa place).</p>

<blockquote>
  <p>Les données reçues sont toujours sous le format <a href="http://fr.wikipedia.org/wiki/JavaScript_Object_Notation">JSON</a>, ce qui permet de débugger assez facilement d’éventuels problèmes.</p>
</blockquote>

<p>La première partie de la méthode permet de <em>parser</em> la réponse. Il suffit juste de tester le <strong>sku</strong> avec les différents id, et voir lequel match. Ainsi, vous saurez que l’item a été acheté.</p>

<blockquote>
  <p>Lors d’un achat sur le <em>Google Play Store</em>, le serveur garde une trace de l’achat ce qui empêche un nouvel achat de l’item en question. Pour palier à cela, vous devez <em>consumer</em> l’achat en question grâce à la ligne en commentaire :</p>
  
  <pre class="prettyprint"><code class=" hljs avrasm">//int response = BillingManager<span class="hljs-preprocessor">.getInstance</span>()<span class="hljs-preprocessor">.getmService</span>()<span class="hljs-preprocessor">.consumePurchase</span>(<span class="hljs-number">3</span>, packageName, purchaseToken)<span class="hljs-comment">;</span></code></pre>
  
  <p>Il vous suffit donc simplement de la dé-commenter.</p>
</blockquote>

<h3 id="etape-bonus-methode-utile">Etape bonus : Méthode utile</h3>

<p>La prochaine méthode est assez utile car elle vous permet d’interroger le serveur pour savoir si un item a déjà été acheté ou non. <br>
Retournez dans votre class <strong>BillingManager</strong> et créez-y la méthode <strong>getPurchaseStatus</strong> qui prendra en paramètre l’identifiant de l’item dont vous voulez connaître l’état : </p>



<pre class="prettyprint"><code class=" hljs cs"><span class="hljs-keyword">private</span> <span class="hljs-keyword">void</span> <span class="hljs-title">getPurchaseStatus</span>(String idProduct) {
    <span class="hljs-keyword">try</span> {
        Bundle ownedItems = <span class="hljs-keyword">null</span>;
        ownedItems = mService.getPurchases(<span class="hljs-number">3</span>, activity.getPackageName(), <span class="hljs-string">"inapp"</span>, <span class="hljs-keyword">null</span>);
        <span class="hljs-keyword">int</span> response = ownedItems.getInt(<span class="hljs-string">"RESPONSE_CODE"</span>);
        <span class="hljs-keyword">if</span> (response == <span class="hljs-number">0</span>) {
            ArrayList&lt;String&gt; ownedSkus = ownedItems.getStringArrayList(<span class="hljs-string">"INAPP_PURCHASE_ITEM_LIST"</span>);

            <span class="hljs-keyword">for</span> (<span class="hljs-keyword">int</span> i = <span class="hljs-number">0</span>; i &lt; ownedSkus.size(); ++i) {
                String sku = ownedSkus.<span class="hljs-keyword">get</span>(i);
                <span class="hljs-keyword">if</span> (sku.equalsIgnoreCase(idProduct)) {
                    <span class="hljs-comment">// Do something here...</span>
                }
            }
        }
    } <span class="hljs-keyword">catch</span> (RemoteException e) {
        e.printStackTrace();
    }
}</code></pre>

<p>Avec cette méthode, vous pouvez donc savoir si vous avez ou non réalisé l’achat de l’item <strong>idProduct</strong>.</p>

<blockquote>
  <p><i class="icon-attention"></i> Si l’achat a été consumé, il n’apparaîtra bien évidemment pas dans la liste récupérée.</p>
</blockquote>



<h2 id="partie-4-exemple-dutilisation">Partie 4 : Exemple d’utilisation</h2>

<p>Il est temps d’utiliser notre <em>manager</em>. Pour cela, il suffit de vous placer dans l’activité où se réalisera l’achat (pour le retour via <strong>onActivityResult</strong> vu plus haut).</p>



<pre class="prettyprint"><code class=" hljs java"><span class="hljs-javadoc">/** ... **/</span>
<span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">launchPurchase</span>() {
    BillingManager.getInstance().setActivity(<span class="hljs-keyword">this</span>);
    BillingManager.getInstance().Initialize();
    BillingManager.getInstance().tryToPurchase(<span class="hljs-string">"myProductId"</span>);
}
<span class="hljs-javadoc">/** ... **/</span></code></pre>

<p>Il vous suffit d’attendre la réponse et de gérer le comportement en question.</p>

<blockquote>
  <p><i class="icon-attention"></i> Le seul moyen de tester l’<em>InApp Billing</em> est de publier une version officielle de l’application sur le <em>Google Play Store</em> (une version Alpha ou Beta suffit). <br>
  De plus, la publication est une procédure longue (jusqu’à plusieurs heures). Soyez donc patients et vérifiez votre code avant de publier votre application et la tester.</p>
</blockquote>

<hr>

<p>Adrien Fenech</p>
