<p>Adrien Fenech</p>

<hr>



<h1 id="android-tuto-3-textview-personnalisee">Android Tuto 3 : TextView personnalisée</h1>



<h2 id="introduction">Introduction</h2>

<p>Ce tutoriel nécessite les connaissances de bases en programmation avec le SDK Android. Ce tutoriel vous permettra de créer une class générique qui s’occupera de la mise en forme de vos <strong>TextView</strong>, en ajoutant par exemple de la couleur, une image ou encore un style particulier au milieu d’un texte.</p>

<blockquote>
  <p><i class="icon-checked"></i> Vous pouvez également consulter ce tutoriel via le <a href="https://stackedit.io/viewer#!provider=gist&gistId=2092376632914df6d400&filename=TutoAndroid_3_TextOptions.md">viewer de StackEdit</a><./p>
</blockquote>

<p><div class="toc">
<ul>
<li><a href="#android-tuto-3-textview-personnalisee">Android Tuto 3 : TextView personnalisée</a><ul>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#partie-1-creation-de-la-class-textoptions">Partie 1 : Création de la class TextOptions</a><ul>
<li><a href="#creation-de-la-class">Création de la class</a></li>
<li><a href="#initialisation-et-construction-de-la-textview">Initialisation et Construction de la TextView</a></li>
<li><a href="#creations-des-options">Créations des options</a><ul>
<li><a href="#coloration-du-texte">Coloration du texte</a></li>
<li><a href="#ajout-dun-style">Ajout d’un style</a></li>
<li><a href="#ajout-dune-image">Ajout d’une image</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="#partie-2-exemple-dutilisation">Partie 2 : Exemple d’utilisation</a></li>
</ul>
</li>
</ul>
</div>
</p>



<h2 id="partie-1-creation-de-la-class-textoptions">Partie 1 : Création de la class <strong>TextOptions</strong></h2>

<p>Notre class aura la forme d’un <em>Singleton</em> afin de pouvoir l’utiliser régulièrement sans avoir à l’instancier à chaque fois.</p>



<h3 id="creation-de-la-class">Création de la class</h3>

<p>Voici l’implémentation de  <strong>TextOptions</strong> :</p>



<pre class="prettyprint"><code class=" hljs java"><span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">TextOptions</span> {</span>
    <span class="hljs-keyword">private</span> <span class="hljs-keyword">static</span> TextOptions ourInstance = <span class="hljs-keyword">new</span> TextOptions();

    <span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> TextOptions <span class="hljs-title">getInstance</span>() {
        <span class="hljs-keyword">return</span> ourInstance;
    }

    <span class="hljs-keyword">private</span> <span class="hljs-title">TextOptions</span>() {
    }

    <span class="hljs-javadoc">/** A/D
    *
    */</span>
    <span class="hljs-keyword">private</span> SpannableStringBuilder ssb;
    <span class="hljs-keyword">private</span> TextView t;
}</code></pre>

<p>Voici l’architecture basique d’un <em>singleton</em>. Rien de spécial à développer, le seul moyen d’accéder à l’unique <em>instance</em> de <strong>TextOptions</strong> est de passer par la méthode <strong>getInstance</strong>.</p>

<p>Voyons maintenant les attributs :</p>



<pre class="prettyprint"><code class=" hljs cs"><span class="hljs-keyword">private</span> SpannableStringBuilder ssb;</code></pre>

<p>C’est un <strong>SpannableStringBuilder</strong> qui s’occupera de formater notre <em>String</em> que nous insérerons dans une <strong>TextView</strong>. Nous allons voir comment l’utiliser dans la suite du tutoriel.</p>



<pre class="prettyprint"><code class=" hljs cs"><span class="hljs-keyword">private</span> TextView t;</code></pre>

<p>Il s’agit bien évidemment de la <strong>TextView</strong> qui possédera les différentes modifications que nous apporterons à notre texte.</p>



<h3 id="initialisation-et-construction-de-la-textview">Initialisation et Construction de la <strong>TextView</strong></h3>

<p>Deux méthodes sont nécessaires pour créer la <strong>TextView</strong> voulue :</p>



<pre class="prettyprint"><code class=" hljs cs"><span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">resetOptions</span>(TextView t, String text) {
    ssb = <span class="hljs-keyword">new</span> SpannableStringBuilder(text);
    t.setText(text);
    <span class="hljs-keyword">this</span>.t = t;
}

<span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">build</span>(){
    t.setText(ssb, TextView.BufferType.SPANNABLE);
}</code></pre>

<ul>
<li><strong>resetOptions</strong> permet d’attibuer une nouvelle <strong>TextView</strong> à travailler ainsi que le texte à modifier.</li>
<li><strong>build</strong> permet d’appliquer les différents styles / options voulus à la <strong>TextView</strong> entrée dans <strong>resetOptions</strong></li>
</ul>



<h3 id="creations-des-options">Créations des options</h3>

<p>Nous allons voir 3 options / styles que nous appliquerons à notre texte. Libre à vous d’en rajouter en suivant le même modèle.</p>



<h4 id="coloration-du-texte">Coloration du texte</h4>



<pre class="prettyprint"><code class=" hljs cs"><span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">addOption</span>(<span class="hljs-keyword">int</span> color, <span class="hljs-keyword">int</span> start, <span class="hljs-keyword">int</span> end) {
    ssb.setSpan(<span class="hljs-keyword">new</span> ForegroundColorSpan(color), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
}</code></pre>

<p>Pas grand chose à dire : cette méthode va appliquer la couleur <strong>color</strong> à partir de l’index <strong>start</strong> jusqu’à l’index <strong>end</strong>. N’hésitez pas à modifier le type de <em>Span</em> pour mettre <strong>start</strong> et <strong>end</strong> inclus ou exclus.</p>



<h4 id="ajout-dun-style">Ajout d’un style</h4>



<pre class="prettyprint"><code class=" hljs cs"><span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">addOption</span>(<span class="hljs-keyword">int</span> start, <span class="hljs-keyword">int</span> end, StyleSpan styleSpan) {
    ssb.setSpan(styleSpan, start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
}</code></pre>

<p>Avec cette méthode, vous pouvez appliquer un style à la portion de texte (<em>Italic</em>, <strong>bold</strong> ou <strong><em>bold&amp;italic</em></strong>). Nous verrons comment lors de l’exemple.</p>



<h4 id="ajout-dune-image">Ajout d’une image</h4>



<pre class="prettyprint"><code class=" hljs cs"><span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">addOption</span>(Bitmap b, <span class="hljs-keyword">int</span> location) {
    ssb.insert(location, <span class="hljs-string">" "</span>);
    ssb.setSpan(<span class="hljs-keyword">new</span> ImageSpan(b), location, location + <span class="hljs-number">1</span>, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
}</code></pre>

<p>Dans cette dernière option, nous ajoutons d’abord un <em>espace blanc</em> au niveau de la position de l’image, afin que le texte ne soit pas remplacé. Ensuite, nous créons simplement un <strong>ImageSpan</strong> à l’aide du <strong>Bitmap</strong> en argument. Puis nous l’ajoutons à notre <strong>SpannableStringBuilder</strong>.</p>



<h2 id="partie-2-exemple-dutilisation">Partie 2 : Exemple d’utilisation</h2>

<p>Voyons maintenant l’utilisation de notre class <strong>TextOptions</strong> au travers d’un exemple :</p>



<pre class="prettyprint"><code class=" hljs avrasm">TextView tv = (TextView)findViewById(R<span class="hljs-preprocessor">.id</span><span class="hljs-preprocessor">.myTextView</span>)<span class="hljs-comment">;</span>
String s = <span class="hljs-string">"Ceci est un exemple pour la class TextOptions !"</span><span class="hljs-comment">;</span>

TextOptions textOptions = TextOptions<span class="hljs-preprocessor">.getInstance</span>()<span class="hljs-comment">;</span>
textOptions<span class="hljs-preprocessor">.resetOptions</span>(tv, s)<span class="hljs-comment">;</span>

textOptions<span class="hljs-preprocessor">.addOption</span>(<span class="hljs-number">5</span>, <span class="hljs-number">8</span>, Color<span class="hljs-preprocessor">.GREEN</span>)<span class="hljs-comment">;</span>

Bitmap b = BitmapFactory<span class="hljs-preprocessor">.decodeResource</span>(getResources(), R<span class="hljs-preprocessor">.drawable</span><span class="hljs-preprocessor">.ic</span>_launcher)<span class="hljs-comment">;</span>
textOptions<span class="hljs-preprocessor">.addOption</span>(b, <span class="hljs-number">20</span>)<span class="hljs-comment">;</span>

textOptions<span class="hljs-preprocessor">.addOption</span>(<span class="hljs-number">35</span>, <span class="hljs-number">43</span>, new StyleSpan(Typeface<span class="hljs-preprocessor">.BOLD</span>))<span class="hljs-comment">;</span>
textOptions<span class="hljs-preprocessor">.addOption</span>(<span class="hljs-number">39</span>, <span class="hljs-number">46</span>, Color<span class="hljs-preprocessor">.RED</span>)<span class="hljs-comment">;</span>

textOptions<span class="hljs-preprocessor">.build</span>()<span class="hljs-comment">;</span></code></pre>

<p>Comme vous pouvez le constater, tout est assez intuitif. En ce qui concerne les styles, il vous suffit de créer un objet <strong>StyleSpan</strong> ayant pour argument une <strong>Typeface</strong> en particulier. Vous pouvez également préciser plusieurs options, et plusieurs fois la même (la couleur dans notre exemple). Voici un aperçu du résultat : </p>

<p><img src="https://raw.githubusercontent.com/adrienfenech/Tutorial/master/save1.png" alt="" title=""></p>

<hr>

<p>Adrien Fenech</p>
